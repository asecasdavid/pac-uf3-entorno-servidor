<?php

/*
 * Función para realizar la conexión con la bd. Se llama desde todas las demás funciones.
 */
function conn() {
    return new mysqli("localhost", "root", "developer", "pac3_daw");
}

/*
 * Función para actualizar la fecha de último acceso del usuario.
 */
function updateDate($date, $user) {
    $mysqli = conn();
    $mysqli->query("UPDATE user SET LastAccess = '$date' WHERE UserID = $user");
}

/*
 * Función para consultar el usuario dado su email y su nombre.
 */
function getUser($email, $user) {
    $mysqli = conn();
    return $mysqli->query("SELECT UserID AS id, FullName AS name, Enabled FROM user WHERE Email = '$email' AND Fullname = '$user'")->fetch_assoc();
}

/*
 * Función para consultar el ID del administrador. Retorna el ID del array asociativo.
 */
function getAdminID() {
    $mysqli = conn();
    return $mysqli->query("SELECT SuperAdmin AS id FROM setup")->fetch_assoc()['id'];
}

/*
 * Función para obtener todos los productos con límite que se calcula dependiendo de la página en la que esté. Realizamos
 * un left join para obtener los nombres de las categorías en la misma query.
 */
function getProducts($page) {
    $mysqli = conn();
    $limit = $page * 10;
    return $mysqli->query("SELECT ProductID AS id, category.Name AS category, product.Name AS name, Cost AS cost, Price AS price FROM product LEFT JOIN category ON product.CategoryID = category.CategoryID ORDER BY ID LIMIT $limit,10")->fetch_all(MYSQLI_ASSOC);
}

/*
 * Función para obtener los valores de un producto en concreto dado un ID.
 */
function getProductValues($id) {
    $mysqli = conn();
    return $mysqli->query("SELECT * FROM product  WHERE ProductID = $id")->fetch_assoc();
}

/*
 * Función para consultar todas las categorías y recorrerlas luego para montar el selector.
 */
function getCategories() {
    $mysqli = conn();
    return $mysqli->query("SELECT * FROM category")->fetch_all(MYSQLI_ASSOC);
}

/*
 * Función para obtener los usuarios y montar la vista con sus valores, ordenados según la columna seleccionada.
 */
function getUsers($order) {
    $mysqli = conn();
    return $mysqli->query("SELECT UserID as id, FullName AS name, Email AS email, LastAccess AS date, Enabled as enabled FROM user ORDER BY $order")->fetch_all(MYSQLI_ASSOC);
}

/*
 * Función para consultar los valores de un usuario en concreto dado su ID.
 */
function getUserValues($id) {
    $mysqli = conn();
    return $mysqli->query("SELECT * FROM user WHERE UserID = $id")->fetch_assoc();
}

/*
 * Función para crear o editar un usuario, la query cambiará dependiendo de la operación que se esté realizando.
 * Reutilizamos la función, ya que tanto para insertar como para actualizar los valores recibidos son los mismos.
 */
function createEditUser($id, $email, $pass, $name, $date, $enabled, $op) {
    $mysqli = conn();
    switch ($op) {
        case 'create':
            $mysqli->query("INSERT INTO user (UserID, Email, Password, FullName, LastAccess, Enabled) VALUES ('$id', '$email', '$pass', '$name', '$date', '$enabled')");
            break;
        case 'edit':
            $mysqli->query("UPDATE user SET UserID = '$id', Email = '$email', Password = '$pass', FullName = '$name', LastAccess = '$date', Enabled = '$enabled' WHERE (UserID = '$id')");
            break;
    }
}

/*
 * Función para borrar el usuario dado su ID.
 */
function deleteUser($id) {
    $mysqli = conn();
    $mysqli->query("DELETE FROM user WHERE (UserID = '$id')");
}

/*
 * Función para crear o editar un producto, la query cambiará dependiendo de la operación que se esté realizando.
 * Reutilizamos la función, ya que tanto para insertar como para actualizar los valores recibidos son los mismos.
 */
function createEditProduct($id, $category, $name, $cost, $price, $op) {
    $mysqli = conn();
    switch ($op) {
        case 'create':
            $mysqli->query("INSERT INTO product (ProductID, Name, Cost, Price, CategoryID) VALUES ('$id', '$name', '$cost', '$price', '$category')");
            break;
        case 'edit':
            $mysqli->query("UPDATE product SET ProductID = '$id', Name = '$name', Cost = '$cost', Price = '$price', CategoryID = '$category' WHERE (`ProductID` = '$id')");
            break;
    }
}

/*
 * Función para borrar el producto dado su ID.
 */
function deleteProduct($id) {
    $mysqli = conn();
    $mysqli->query("DELETE FROM product WHERE (ProductID = '$id')");
}