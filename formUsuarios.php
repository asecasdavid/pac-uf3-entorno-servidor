<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>formUsuarios.php</title>
    <link rel="stylesheet" href="main.css">
    <script>
        <?php
            // Incluimos el fichero con las querys.
            include "basedatos.php";
            session_start();

            // Pequeña función para añadir el parámetro hidden al formulario y que se oculte cuando hacemos submit.
            function hideForm() {
                if (!empty($_POST)) {
                    echo 'hidden';
                }
            }
        ?>
    </script>
</head>
<body>
    <?php
        // Comprobamos si se ha hecho submit, y que operación se ha hecho para hacer la query correspondiente.
        if (!empty($_POST)) {
            // Generamos el mensaje y el botón de volver tras crear, editar o eliminar.
            echo "<div class='form-wrapper'>";
            switch ($_POST['op']) {
                case 'create':
                    createEditUser($_POST['id'], $_POST['email'], $_POST['pass'], $_POST['name'], $_POST['date'], $_POST['enabled'], $_POST['op']);
                    echo "<p>Se ha creado el usuario</p>";
                    break;
                case 'edit':
                    createEditUser($_POST['id'], $_POST['email'], $_POST['pass'], $_POST['name'], $_POST['date'], $_POST['enabled'], $_POST['op']);
                    echo "<p>Se ha modificado el usuario</p>";
                    break;
                case 'delete':
                    deleteUser($_POST['id']);
                    echo "<p>se ha borrado el usuario</p>";
                    break;
            }
            echo "<div class='center'><a class='button button-small' href='usuarios.php'>Volver</a></div>";
            echo "</div>";
        }
    ?>

    <!-- Generamos el formulario y llamamos a la función que lo oculta cuando se hace submit -->
    <form action="formUsuarios.php" method="post" <?php hideForm() ?>>
        <div class="form-wrapper">
            <?php
                /*
                 * Obtenemos la operación que se va a realizar y los valores del usuario que vendrán solo si se está
                 * modificando o eliminando.
                 */
                $op = $_GET['op'];
                $user = isset($_GET['user']) ? getUserValues($_GET['user']) : NULL;

                // Mostramos un mensaje diferente en función de la operación que se vaya a realizar.
                switch ($op) {
                    case 'create':
                        echo '<h2 class="center"><strong>Se va a añadir un usuario nuevo</strong></h2>';
                        break;
                    case 'edit':
                        echo '<h2 class="center"><strong>Se va a editar el usuario seleccionado</strong></h2>';
                        break;
                    case 'delete':
                        echo '<h2 class="center"><strong>Se va a eliminar el usuario seleccionado</strong></h2>';
                        break;
                }
            ?>
            <!-- Generamos todos los campos del formulario, si tenemos usuario, la operación será edición o eliminación
            así que estableceremos los valores por defecto -->
            <div class="form-element">
                <label class="inline-label" for="id">ID:</label>
                <?php
                    if ($user) {
                        echo "<input type='number' id='id' name='id' required value='$user[UserID]'>";
                    }
                    else {
                        echo "<input type='number' id='id' name='id' required>";
                    }
                ?>
            </div>
            <div class="form-element">
                <label class="inline-label" for="name">Nombre:</label>
                <?php
                    if ($user) {
                        echo "<input type='text' id='name' name='name' required  value='$user[FullName]'>";
                    }
                    else {
                        echo "<input type='text' id='name' name='name' required>";
                    }
                ?>
            </div>
            <div class="form-element">
                <label class="inline-label" for="pass">Contraseña:</label>
                <?php
                if ($user) {
                    echo "<input type='password' id='pass' name='pass' required  value='$user[Password]'>";
                }
                else {
                    echo "<input type='password' id='pass' name='pass' required>";
                }
                ?>
            </div>
            <div class="form-element">
                <label class="inline-label" for="email">Correo:</label>
                <?php
                if ($user) {
                    echo "<input type='email' id='email' name='email' required  value='$user[Email]'>";
                }
                else {
                    echo "<input type='email' id='email' name='email' required>";
                }
                ?>
            </div>
            <div class="form-element">
                <label class="inline-label" for="date">Último acceso:</label>
                <?php
                if ($user) {
                    echo "<input type='date' id='date' name='date' required  value='$user[LastAccess]'>";
                }
                else {
                    echo "<input type='date' id='date' name='date' required>";
                }
                ?>
            </div>
            <div>
                <!-- Para el campo tipo radio comprobamos si el usuario esta enabled o no para marcarlo como opción por
                defecto cuando se edita o elimina el usuario -->
                <div class="form-element-radio">
                    <p class="inline-label">Autorizado:</p>
                    <?php
                        $checked = isset($user['Enabled']) ? $user['Enabled'] : NULL;
                        if ($user && $op == 'delete' || $op == 'edit'&& $checked == 1) {
                            echo "<input type='radio' id='enabled' name='enabled' value='1' checked='checked'>";
                        }
                        else {
                            echo "<input type='radio' id='enabled' name='enabled' value='1'>";
                        }
                    ?>
                    <label class="inline-label" for="enabled">Sí</label>
                    <?php
                        if ($user && $op == 'delete' || $op == 'edit' && $checked == 0) {
                            echo "<input type='radio' id='disabled' name='enabled' value='0' checked='checked'>";
                        }
                        else {
                            echo "<input type='radio' id='disabled' name='enabled' value='0'>";
                        }
                    ?>
                    <label class="inline-label" for="disabled">No</label>
                </div>
            </div>
            <div>
                <!-- Generamos el botón en con el valor de la operación que se va a realizar para poder identificarla -->
                <a href="usuarios.php" class="button button-small left">Volver</a>
                <?php
                    switch ($op) {
                        case 'create':
                            echo "<button class='button button-small right' name='op' type='submit' value='create'>Añadir</button>";
                            break;
                        case 'edit':
                            echo "<button class='button button-small right' name='op' type='submit' value='edit'>Editar</button>";
                            break;
                        case 'delete':
                            echo "<button class='button button-small right' name='op' type='submit' value='delete'>Eliminar</button>";
                            break;
                    }
                ?>
            </div>
        </div>
    </form>
</body>
</html>