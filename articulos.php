<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>artículos.php</title>
    <link rel="stylesheet" href="main.css">
    <script>
        <?php
            // Incluimos el fichero con las querys.
            include "basedatos.php";
            session_start();
        ?>
    </script>
</head>
<body>
    <div class="form-wrapper">
        <?php
            // Si es admin o autorizado mostramos botón para crear productos.
            if ($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'authorized') {
                echo "<a class='button button-small left' href='formArticulos.php?op=create'>Crear nuevo producto</a>";
            }
        ?>
        <table>
            <tr>
                <?php
                    // Obtenemos la página actual del usuario, si no está en ninguna, establecemos la 0.
                    $currentPage = $_GET['page'] ?? 0;

                    /*
                     * Para la paginación utilizamos el parámetro sort con el nombre de la columna por la que se vaya
                     * a ordenar y luego lo utilizamos en la query. También cogemos la página actual en la que está
                     * para mostrar el enlace sobre esa misma página, pero con diferente ordenación.
                     */
                    echo "<th><a href='?page=$currentPage&sort=id'>ID</a></th>";
                    echo "<th><a href='?page=$currentPage&sort=category'>Categoría</a></th>";
                    echo "<th><a href='?page=$currentPage&sort=name'>Nombre</a></th>";
                    echo "<th><a href='?page=$currentPage&sort=cost'>Coste</a></th>";
                    echo "<th><a href='?page=$currentPage&sort=price'>Precio</a></th>";

                    // Solo si es admin mostramos los botones de manejo.
                    if ($_SESSION['role'] == 'admin') {
                        echo '<th>Manejo</th>';
                    }
                ?>
            </tr>
            <?php
                /*
                 * Obtenemos la página actual si existe, si no establecemos que está en la página 1, por defecto,
                 * ordenamos la tabla por ID, a no ser que nos venga un parámetro por url, entonces lo ordenamos
                 * en función del parámetro. Realizamos la query con la página actual y la ordenación seleccionada.
                 */
                $sort = $_GET['sort'] ?? 'id';

                // Obtenemos los artículos dependiendo de la página y ordenados por la columna que se haya seleccionado.
                $articles = getProducts($currentPage, $sort);

                /*
                 * Primero generamos un array con la columna por la que se va a ordenar, luego con array_multisort,
                 * aplicamos la ordenación de ese array al array con los artículos, utilizando la columna seleccionada
                 * y los flags para que no sea sensible a maýusculas y minúsculas.
                 */
                $order = array_column($articles, $sort);
                array_multisort($order, SORT_ASC, SORT_NATURAL|SORT_FLAG_CASE, $articles);

                foreach ($articles as $article) {
                    echo "<tr>";
                    echo "<td>$article[id]</td>";
                    echo "<td>$article[category]</td>";
                    echo "<td>$article[name]</td>";
                    echo "<td>$article[cost]</td>";
                    echo "<td>$article[price]</td>";

                    /*
                     * Si es el admin mostramos los botones de editar y eliminar con las imágenes y pasamos el producto
                     * y la operación que se va a realizar.
                     */
                    if ($_SESSION['role'] == 'admin') {
                        echo "<td><a class='op-button' href='formArticulos.php?op=edit&product=$article[id]'>
                              <img alt='button' class='op-button' src='images/edit.png'></a>
                              <a href='formArticulos.php?op=delete&product=$article[id]'>
                              <img alt='button' class='op-button' src='images/delete.png'></a></td>";
                    }
                    echo "</tr>";
                }
            ?>
        </table>
        <?php
            /*
             * Si está en la primera página, solo mostramos el botón de siguiente, siempre que haya 10 o más artículos,
             * si está en otra página y hay 10 o más mostramos ambos botones y si hay menos de 10 mostramos solo el botón
             * de volver a la página anterior. Esto lo hago para evitar pasar página donde no haya artículos y para
             * evitar volver a una página anterior si está en la primera.
             */
            $articles = count(getProducts($currentPage, 'id'));

            if ($currentPage == 0 && $articles >= 10) {
                $nextPage = $currentPage + 1;
                echo "<a class='button button-small right' href='articulos.php?page=$nextPage'>>>></a>";
            }
            elseif ($currentPage > 0 && $articles >= 10) {
                $nextPage = $currentPage + 1;
                $prevPage = $currentPage - 1;
                echo "<a class='button button-small left' href='articulos.php?page=$prevPage'><<<</a>";
                echo "<a class='button button-small right' href='articulos.php?page=$nextPage'>>>></a>";
            }
            elseif ($currentPage > 0 && $articles < 10) {
                $prevPage = $currentPage - 1;
                echo "<a class='button button-small left' href='articulos.php?page=$prevPage'><<<</a>";
            }
        ?>
    </div>
</body>
</html>