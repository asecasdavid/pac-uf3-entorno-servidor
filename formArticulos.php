<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>formArticulos.php</title>
    <link rel="stylesheet" href="main.css">
    <script>
        <?php
            // Incluimos el fichero con las querys.
            include "basedatos.php";
            session_start();

            // Pequeña función para añadir el parámetro hidden al formulario y que se oculte cuando hacemos submit.
            function hideForm() {
                if (!empty($_POST)) {
                    echo 'hidden';
                }
            }
        ?>
    </script>
</head>
<body>
    <?php
        // Comprobamos si se ha hecho submit, y que operación se ha hecho para hacer la query correspondiente.
        if (!empty($_POST)) {
            // Generamos el mensaje y el botón de volver tras crear, editar o eliminar.
            echo "<div class='form-wrapper'>";
            switch ($_POST['op']) {
                case 'create':
                    createEditProduct($_POST['id'], $_POST['category'], $_POST['name'], $_POST['cost'], $_POST['price'], $_POST['op']);
                    echo "<p>Se ha creado el artículo</p>";
                    break;
                case 'edit':
                    createEditProduct($_POST['id'], $_POST['category'], $_POST['name'], $_POST['cost'], $_POST['price'], $_POST['op']);
                    echo "<p>Se ha modificado el artículo</p>";
                    break;
                case 'delete':
                    deleteProduct($_POST['id']);
                    echo "<p>se ha borrado el artículo</p>";
                    break;
            }
            echo "<div class='center'><a class='button button-small' href='articulos.php'>Volver</a></div>";
            echo "</div>";
        }
    ?>

    <!-- Generamos el formulario y llamamos a la función que lo oculta cuando se hace submit -->
    <form action="formArticulos.php" method="post" <?php hideForm() ?>>
        <div class="form-wrapper">
            <?php
                /*
                 * Obtenemos la operación que se va a realizar y los valores del artículo que vendrán solo si se está
                 * modificando o eliminando.
                 */
                $op = $_GET['op'];
                $product = isset($_GET['product']) ? getProductValues($_GET['product']) : NULL;

                // Mostramos un mensaje diferente en función de la operación que se vaya a realizar.
                switch ($op) {
                    case 'create':
                        echo '<h2 class="center"><strong>Se va a añadir un artículo nuevo</strong></h2>';
                        break;
                    case 'edit':
                        echo '<h2 class="center"><strong>Se va a editar el artículo seleccionado</strong></h2>';
                        break;
                    case 'delete':
                        echo '<h2 class="center"><strong>Se va a eliminar el artículo seleccionado</strong></h2>';
                        break;
                }
            ?>
            <!-- Generamos todos los campos del formulario, si tenemos producto, la operación será edición o eliminación
            así que estableceremos los valores por defecto -->
            <div class="form-element">
                <label class="inline-label" for="id">ID:</label>
                <?php
                    if ($product) {
                        echo "<input type='number' id='id' name='id' required value='$product[ProductID]'>";
                    }
                    else {
                        echo "<input type='number' id='id' name='id' required>";
                    }
                ?>
            </div>
            <!-- Montamos el select con todas las categorías y como value su id -->
            <div class="form-element">
                <label class="inline-label" for="category">Categoría:</label>
                    <?php
                        echo "<select name='category' id='category'>";

                        // Obtenemos todas las categorías con su nombre y su id.
                        $categories = getCategories();

                        // Recorremos las categorías montando las options.
                        foreach ($categories as $category) {
                            if (isset($product['CategoryID']) && $product['CategoryID'] == $category['CategoryID']) {
                                echo "<option value='$category[CategoryID]' selected>$category[Name]</option>";
                            }
                            else {
                                echo "<option value='$category[CategoryID]'>$category[Name]</option>";
                            }
                        }
                        echo "</select>";
                    ?>
            </div>
            <div class="form-element">
                <label class="inline-label" for="pass">Nombre:</label>
                <?php
                if ($product) {
                    echo "<input type='text' id='name' name='name' required  value='$product[Name]'>";
                }
                else {
                    echo "<input type='text' id='name' name='name' required>";
                }
                ?>
            </div>
            <div class="form-element">
                <label class="inline-label" for="cost">Coste:</label>
                <?php
                if ($product) {
                    echo "<input type='number' id='cost' name='cost' required value='$product[Cost]'>";
                }
                else {
                    echo "<input type='number' id='cost' name='cost' required>";
                }
                ?>
            </div>
            <div class="form-element">
                <label class="inline-label" for="price">Precio:</label>
                <?php
                if ($product) {
                    echo "<input type='number' id='price' name='price' required  value='$product[Price]'>";
                }
                else {
                    echo "<input type='number' id='price' name='price' required>";
                }
                ?>
            </div>
            <div>
                <!-- Generamos el botón en con el valor de la operación que se va a realizar para poder identificarla -->
                <a href="articulos.php" class="button button-small left">Volver</a>
                <?php
                    switch ($op) {
                        case 'create':
                            echo "<button class='button button-small right' name='op' type='submit' value='create'>Añadir</button>";
                            break;
                        case 'edit':
                            echo "<button class='button button-small right' name='op' type='submit' value='edit'>Editar</button>";
                            break;
                        case 'delete':
                            echo "<button class='button button-small right' name='op' type='submit' value='delete'>Eliminar</button>";
                            break;
                    }
                ?>
            </div>
        </div>
    </form>
</body>
</html>