<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>acceso.php</title>
    <link rel="stylesheet" href="main.css">
    <script>
        <?php
            // Incluimos el fichero con las querys.
            include "basedatos.php";
            session_start();
        ?>
    </script>
</head>
<body>
    <div class="form-wrapper">
        <div class="center">
            <a class="button" href="articulos.php">Artículos</a>
            <?php
                // Mostramos el enlace a usuarios solo si es el admin.
                if ($_SESSION['role'] == 'admin') {
                    echo "<a class='button' href='usuarios.php'>Usuarios</a>";
                }
            ?>
        </div>
        <div class="center">
            <a class="button" href="index.php">Volver</a>
        </div>
    </div>
</body>
</html>