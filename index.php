<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>index.php</title>
    <link rel="stylesheet" href="main.css">
    <script>
        <?php
            // Incluimos el fichero con las querys.
            include "basedatos.php";
            session_start();
        ?>
    </script>
</head>
<body>
    <!-- Creamos el formulario -->
    <form action="index.php" method="post">
        <!-- Clases para reutilizar estilos en todas las páginas -->
        <div class="form-wrapper">
            <div class="login-form center">
                <label for="name">Usuario:</label>
                <input type="text" id="name" name="name" required>
            </div>
            <div class="login-form center">
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" required>
            </div>
            <div class="center">
                <input type="submit" value="Acceder">
            </div>
                <?php
                    // Comprobamos los resultados tras el submit.
                    if (isset($_POST['name']) && $_POST['email']) {
                        $user = $_POST['name'];
                        $email = $_POST['email'];

                        // Obtenemos los datos del usuario si existiera.
                        $results = getUser($email, $user);

                        /*
                         * Si el usuario existe, creamos la sesión, mostramos mensaje de acceso, y actualizamos la fecha
                         * de último acceso a la actual.
                         */
                        if ($results) {
                            $_SESSION['id'] = $results['id'];
                            $_SESSION['name'] = $results['name'];
                            $loginMsg = "<p class='login-msg'>Bienvenido $user, pulsa <a class='login-link text-red' href='acceso.php'>aquí</a> para continuar.</p>";

                            // Obtenemos la fecha con formato y la establecemos al usuario.
                            $date = date('Y-m-d');
                            updateDate($date, $results['id']);

                            /*
                             * Primero comprobamos si el ID coincide con el del admin, si no, comprobamos si es un
                             * usuario autorizado o registrado.
                             */
                            if (getAdminID() == $results['id']) {
                                $_SESSION['role'] = 'admin';
                                echo $loginMsg;
                            }
                            elseif ($results['Enabled'] == 1) {
                                $_SESSION['role'] = 'authorized';
                                echo $loginMsg;
                            }
                            elseif ($results['Enabled'] == 0) {
                                $_SESSION['role'] = 'registered';
                                echo $loginMsg;
                            }
                        }
                        else {
                            // Si no existe mostramos mensaje de error.
                            echo "<p class='login-msg text-red'>El usuario/email no es correcto o no existe</p>";
                        }
                    }
                ?>
        </div>
    </form>
</body>
</html>