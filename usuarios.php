<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>usuarios.php</title>
    <link rel="stylesheet" href="main.css">
    <script>
        <?php
            // Incluimos el fichero con las querys.
            include "basedatos.php";
        ?>
    </script>
</head>
<body>
<div class="form-wrapper">
    <a class='button button-small left' href='formUsuarios.php?op=create'>Crear nuevo usuario</a>
    <table>
        <tr>
            <!-- Mostramos todas las columnas como un enlace con el parámetro que utilizamos para ordenarlas -->
            <th><a href='?sort=id'>ID</a></th>
            <th><a href='?sort=name'>Nombre</a></th>
            <th><a href='?sort=email'>Email</a></th>
            <th><a href='?sort=date'>Último acceso</a></th>
            <th><a href='?sort=enabled'>Enabled</a></th>
            <th>Manejo</th>
        </tr>
        <?php
            // Por defecto los ordenamos por ID si no nos viene ningún parámetro.
            $sort = $_GET['sort'] ?? 'id';

            // Obtenemos todos los usuarios y los ordenamos según el parámetro.
            $users = getUsers($sort);

            foreach ($users as $user) {
                // Formateamos la fecha para que sea dd/mm/yy como se indica en el ejercicio.
                $date = date('d/m/y', strtotime($user['date']));

                // Establecemos clases a la fila del admin para resaltarlo y deshabilitarlo.
                $class = '';
                $class2 = '';
                if (getAdminID() == $user['id']) {
                    $class = 'text-red';
                    $class2 = 'disabled';
                }

                // Pasamos en los enlaces de manejo la operación y el usuario que se va a editar o eliminar.
                echo "<tr class='$class'>";
                echo "<td>$user[id]</td>";
                echo "<td>$user[name]</td>";
                echo "<td>$user[email]</td>";
                echo "<td>$date</td>";
                echo "<td>$user[enabled]</td>";
                echo "<td class='$class2'><a class='op-button' href='formUsuarios.php?op=edit&user=$user[id]'>
                      <img alt='button' class='op-button' src='images/edit.png'></a>
                      <a href='formUsuarios.php?op=delete&user=$user[id]'>
                      <img alt='button' class='op-button' src='images/delete.png'></a></td>";
                echo "</tr>";
            }
        ?>
    </table>
</div>
</body>
</html>